# SQUID Server #

TODO: Fork this container: https://hub.docker.com/r/sameersbn/squid and
make it .htpasswd-based:

```
auth_param basic program /usr/lib64/squid/basic_ncsa_auth /etc/squid/.htpasswd
auth_param basic children 5
auth_param basic realm Squid Basic Authentication
auth_param basic credentialsttl 5 hours
acl password proxy_auth REQUIRED
http_access allow password
```